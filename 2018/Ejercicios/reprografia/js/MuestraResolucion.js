function MuestraResolucionIMG1() {
	alert('ancho = ' + document.getElementById('img1').width + 'px' + " | " + 'alto = ' + document.getElementById('img1').height + 'px');
}

function MuestraResolucionIMG2() {
	alert('ancho = ' + document.getElementById('img2').width + 'px' + " | " + 'alto = ' + document.getElementById('img2').height + 'px');
}

function MostrarEnlaceIMG1() {
	alert('https://es.wikipedia.org/wiki/Otariinae');
}

function MostrarEnlaceIMG2() {
	alert('https://es.wikipedia.org/wiki/Psittacoidea');
}

function ResolucionMedia() {
	var alto = 500,
		ancho = 400;

	document.document.getElementsByTagName("META")[0].httpEquiv = "default-style"; 
	document.document.getElementsByTagName("META")[0].content = "0";

	document.getElementById('img1').width = alto; 
	document.getElementById('img1').height = ancho; 

	document.getElementById('img2').width = alto; 
	document.getElementById('img2').height = ancho;  
}

function ResolucionBaja() {
	var alto = 300,
		ancho = 200;

	document.document.getElementsByTagName("META")[0].httpEquiv = "default-style"; 
	document.document.getElementsByTagName("META")[0].content = "0";

	document.getElementById('img1').width = alto; 
	document.getElementById('img1').height = ancho; 

	document.getElementById('img2').width = alto; 
	document.getElementById('img2').height = ancho; 
}