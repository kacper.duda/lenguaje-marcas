function CalcularTabla()
{
	var numero,
			contador,
			resultado;

	numero = parseInt(document.getElementById('numero').value);
  resultado = document.getElementById('resultadoTabla');

	contador = 1;
	resultado.innerHTML = "";
	while (contador <= 10) {
				resultado.innerHTML +=
				"<br>" + numero + " x " + contador + " = " + (numero*contador);
				contador++;
			}
}
/*******************************************************************************/
function CalcularTablaDW()
{
	var numero,
			contador,
			resultado;

	numero = parseInt(document.getElementById('numeroDW').value);
  resultado = document.getElementById('resultadoTablaDW');

	contador = 1;
	resultado.innerHTML = "";
	do {
				resultado.innerHTML +=
				"<br>" + numero + " x " + contador + " = " + (numero*contador);
				contador++;
			}
	while (contador <= 10);
}
/*******************************************************************************/
function CalcularTablaF()
{
	var numero,
			contador,
			resultado;

	numero = parseInt(document.getElementById('numeroF').value);
  resultado = document.getElementById('resultadoTablaF');

	resultado.innerHTML = "";

	for (contador = 1; contador <=10; contador++){
				resultado.innerHTML +=
				"<br>" + numero + " x " + contador + " = " + (numero*contador);
			}
}
/*******************************************************************************/
function EscribirDias(){
	var tablaDias,
			contador,
			resultado;

	tablaDias = new Array();
	tablaDias [0]= "Lunes";
	tablaDias [1]= "Martes";
	tablaDias [2]= "Miercoles";
	tablaDias [3]= "Jueves";
	tablaDias [4]= "Viernes";

	resultado = document.getElementById('resultadoDias');

	resultado.innerHTML = "";

	//Recorer tablaDias con un for
	for (contador=0;contador<5;contador++){
		resultado.innerHTML += "<br>" + tablaDias[contador] ;
	}

	resultado.innerHTML += "<br>";

	//Recorrer tablaDias con for IN
	for (contador in tablaDias){
		resultado.innerHTML += "<br>" + tablaDias[contador];
	}
}
