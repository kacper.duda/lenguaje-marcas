
function tableroAjedrez() {

  // Declaración de variables
  var arrayTableroCampos,
      long = 8, // longitud de las columnas y las filas del tablero
      tablero, 
      color,
      pieza,
      poscol,
      posfila,
      zonaMovCol,
      zonaMovFila;

  var alfilMovCol,
      alfilMovFila,
      reinaMovCol,
      reinaMovFila;

  // Llama a la función que borra el tablero y las piezas
  limpiarTablero();

  // Sirve para coger el div de html a la variable tablero
  tablero = document.getElementById('tablero');

  // Sirve para coger el valor de los select (colo y pieza) en el html
  color = document.getElementById('color').value;
  pieza = document.getElementById('piezas').value;

  // Sirve para coger el valor de los input (poscol y posfila) en el html
  poscol = document.getElementById('poscol').value -1;
  posfila = document.getElementById('posfila').value -1;
  zonaMovCol = document.getElementById('poscol').value -1;
  zonaMovFila = document.getElementById('posfila').value -1;

  // Condición que comprueba si el campo esta vacio o es mayor que 8
  if ( poscol == -1 || posfila == -1 ) {

      alert("Los campos columna y fila deben ser [1-8].");

  } else if ( poscol <= -1 || posfila <= -1 ) {

      alert("Los campos columna y fila deben ser [1-8].");

  } else if ( poscol >= 8 || posfila >= 8 ) {

      alert("Los campos columna y fila deben ser [1-8].");
      return false;

  // En caso contrario ejecuta el resto del código
  } else {

    // Se declara array padre y dentro de este array hijo para declarar las posiciones del tablero
    arrayTableroCampos = new Array(long);
      for (var i=0; i < long; i++)
        arrayTableroCampos[i] = new Array(long);

    // Bucle que recorre el array del padre que son las columnas
    for (var col=0; col < long; col++) {
      // Bucle que recorre los array de los hijos que son las filas
      for (var fila=0; fila < long; fila++) {
        // Condición que coge los números pares de todas las columnas y filas y inserta la imagen negra
        if ( ((fila % 2 == 0) && (col % 2 != 0)) || ((fila % 2 != 0) && (col % 2 == 0)) )
          arrayTableroCampos[col][fila] = '<div class="col:' + col + '-fila:' + fila + '"><img src="img/negro.png"/></div>';
        // Esta condición devuelve que en caso de que no se cumpla la condición de arriba inserta la imagen blanca
        else
          arrayTableroCampos[col][fila] = '<div class="col:' + col + '-fila:' + fila + '"><img src="img/blanco.png"/></div>';
      }
    }

    // CONDICIONES COLOR NEGRO - TORRE
    // Si es color 1 (negro) que ejecute este codigo
    if (color == 1) {

      // pieza 1 = TORRE
      if (pieza == 1) {

        // Añade al tablero las zonas de amenaza de la torre al colocar la figura en X posición
        for (zonaMovCol; zonaMovCol < long; zonaMovCol++)
          arrayTableroCampos[zonaMovCol][posfila] = '<img src="img/rojo.png">';
        for (zonaMovCol = zonaMovCol - 1; zonaMovCol >= 0; zonaMovCol--)
          arrayTableroCampos[zonaMovCol][posfila] = '<img src="img/rojo.png">';
        for (zonaMovFila; zonaMovFila < long; zonaMovFila++)
          arrayTableroCampos[poscol][zonaMovFila] = '<img src="img/rojo.png">';
        for (zonaMovFila = zonaMovFila - 1; zonaMovFila >= 0; zonaMovFila--)
          arrayTableroCampos[poscol][zonaMovFila] = '<img src="img/rojo.png">';

        // Añade la imagen de la TORRE
        arrayTableroCampos[poscol][posfila] = '<img src="img/ntorre.png">';

      }
    // CONDICIONES COLOR BLANCO - TORRE
    // Si es color 1 (blanco) que ejecute este codigo
    } else if (color == 2) {

      // pieza 1 = TORRE
      if (pieza == 1) {

        for (zonaMovCol; zonaMovCol < long; zonaMovCol++)
          arrayTableroCampos[zonaMovCol][posfila] = '<img src="img/rojo.png">';
        for (zonaMovCol = zonaMovCol - 1; zonaMovCol >= 0; zonaMovCol--)
          arrayTableroCampos[zonaMovCol][posfila] = '<img src="img/rojo.png">';
        for (zonaMovFila; zonaMovFila < long; zonaMovFila++)
          arrayTableroCampos[poscol][zonaMovFila] = '<img src="img/rojo.png">';
        for (zonaMovFila = zonaMovFila - 1; zonaMovFila >= 0; zonaMovFila--)
          arrayTableroCampos[poscol][zonaMovFila] = '<img src="img/rojo.png">';

        // Añade la imagen de la TORRE
        arrayTableroCampos[poscol][posfila] = '<img src="img/btorre.png">';

      }
    }

    /* ==================================================================================================== */

    // CONDICIONES COLOR NEGRO - ALFIL
    // Si es color 1 (negro) que ejecute este codigo
    if (color == 1) {

      // pieza 2 = ALFIL
      if (pieza == 2) {

        alfilMovCol = zonaMovCol;
        alfilMovFila = zonaMovFila;

        while ( (zonaMovCol < long) && (zonaMovFila < long) )
          arrayTableroCampos[zonaMovCol++][zonaMovFila++] = '<img src="img/rojo.png">';

        zonaMovCol = alfilMovCol;
        zonaMovFila = alfilMovFila;
        while ( (zonaMovCol >= 0) && (zonaMovFila >= 0) )
          arrayTableroCampos[zonaMovCol--][zonaMovFila--] = '<img src="img/rojo.png">';

        zonaMovCol = alfilMovCol;
        zonaMovFila = alfilMovFila;
        while ( (zonaMovCol >= 0) && (zonaMovFila < long) )
          arrayTableroCampos[zonaMovCol--][zonaMovFila++] = '<img src="img/rojo.png">';

        zonaMovCol = alfilMovCol;
        zonaMovFila = alfilMovFila;
        while ( (zonaMovCol < long) && (zonaMovFila >= 0) )
          arrayTableroCampos[zonaMovCol++][zonaMovFila--] = '<img src="img/rojo.png">';

        // Añade la imagen del ALFIL
        arrayTableroCampos[poscol][posfila] = '<img src="img/nalfil.png">'

      }
    // CONDICIONES COLOR BLANCO - ALFIL
    // Si es color 1 (blanco) que ejecute este codigo
    } else if (color == 2) {

      // pieza 2 = ALFIL
      if (pieza == 2) {

        alfilMovCol = zonaMovCol;
        alfilMovFila = zonaMovFila;

        while ( (zonaMovCol < long) && (zonaMovFila < long) )
          arrayTableroCampos[zonaMovCol++][zonaMovFila++] = '<img src="img/rojo.png">';

        zonaMovCol = alfilMovCol;
        zonaMovFila = alfilMovFila;
        while ( (zonaMovCol >= 0) && (zonaMovFila >= 0) )
          arrayTableroCampos[zonaMovCol--][zonaMovFila--] = '<img src="img/rojo.png">';

        zonaMovCol = alfilMovCol;
        zonaMovFila = alfilMovFila;
        while ( (zonaMovCol >= 0) && (zonaMovFila < long) )
          arrayTableroCampos[zonaMovCol--][zonaMovFila++] = '<img src="img/rojo.png">';

        zonaMovCol = alfilMovCol;
        zonaMovFila = alfilMovFila;
        while ( (zonaMovCol < long) && (zonaMovFila >= 0) )
          arrayTableroCampos[zonaMovCol++][zonaMovFila--] = '<img src="img/rojo.png">';

        // Añade la imagen del ALFIL
        arrayTableroCampos[poscol][posfila] = '<img src="img/balfil.png">';

      }
    }

    /* ==================================================================================================== */

    // CONDICIONES COLOR NEGRO - REINA
    // Si es color 1 (negro) que ejecute este codigo
    if (color == 1) {

      // pieza 3 = REINA
      if (pieza == 3) {

        reinaMovCol = zonaMovCol;
        reinaMovFila = zonaMovFila;
        while ( (zonaMovCol < long) && (zonaMovFila < long) )
          arrayTableroCampos[zonaMovCol++][zonaMovFila++] = '<img src="img/rojo.png">';

        zonaMovCol = reinaMovCol;
        zonaMovFila = reinaMovFila;
        while ( (zonaMovCol >= 0) && (zonaMovFila >= 0) )
          arrayTableroCampos[zonaMovCol--][zonaMovFila--] = '<img src="img/rojo.png">';

        zonaMovCol = reinaMovCol;
        zonaMovFila = reinaMovFila;
        while ( (zonaMovCol >= 0) && (zonaMovFila < long) )
          arrayTableroCampos[zonaMovCol--][zonaMovFila++] = '<img src="img/rojo.png">';

        zonaMovCol = reinaMovCol;
        zonaMovFila = reinaMovFila;
        while ( (zonaMovCol < long) && (zonaMovFila >= 0) )
          arrayTableroCampos[zonaMovCol++][zonaMovFila--] = '<img src="img/rojo.png">';

        for (zonaMovCol; zonaMovCol < long; zonaMovCol++)
          arrayTableroCampos[zonaMovCol][posfila] = '<img src="img/rojo.png">';
        for (zonaMovCol = zonaMovCol - 1; zonaMovCol >= 0; zonaMovCol--)
          arrayTableroCampos[zonaMovCol][posfila] = '<img src="img/rojo.png">';
        for (zonaMovFila; zonaMovFila < long; zonaMovFila++)
          arrayTableroCampos[poscol][zonaMovFila] = '<img src="img/rojo.png">';
        for (zonaMovFila = zonaMovFila - 1; zonaMovFila >= 0; zonaMovFila--)
          arrayTableroCampos[poscol][zonaMovFila] = '<img src="img/rojo.png">';

        // Añade la imagen de la REINA
        arrayTableroCampos[poscol][posfila] = '<img src="img/nreina.png">';    

      }
    // CONDICIONES COLOR BLANCO - REINA
    // Si es color 1 (blanco) que ejecute este codigo
    } else if (color == 2) {

      // pieza 3 = REINA
      if (pieza == 3) {

        reinaMovCol = zonaMovCol;
        reinaMovFila = zonaMovFila;
        while ( (zonaMovCol < long) && (zonaMovFila < long) )
          arrayTableroCampos[zonaMovCol++][zonaMovFila++] = '<img src="img/rojo.png">';

        zonaMovCol = reinaMovCol;
        zonaMovFila = reinaMovFila;
        while ( (zonaMovCol >= 0) && (zonaMovFila >= 0) )
          arrayTableroCampos[zonaMovCol--][zonaMovFila--] = '<img src="img/rojo.png">';

        zonaMovCol = reinaMovCol;
        zonaMovFila = reinaMovFila;
        while ( (zonaMovCol >= 0) && (zonaMovFila < long) )
          arrayTableroCampos[zonaMovCol--][zonaMovFila++] = '<img src="img/rojo.png">';

        zonaMovCol = reinaMovCol;
        zonaMovFila = reinaMovFila;
        while ( (zonaMovCol < long) && (zonaMovFila >= 0) )
          arrayTableroCampos[zonaMovCol++][zonaMovFila--] = '<img src="img/rojo.png">';
        
        for (zonaMovCol; zonaMovCol < long; zonaMovCol++)
          arrayTableroCampos[zonaMovCol][posfila] = '<img src="img/rojo.png">';
        for (zonaMovCol = zonaMovCol - 1; zonaMovCol >= 0; zonaMovCol--)
          arrayTableroCampos[zonaMovCol][posfila] = '<img src="img/rojo.png">';
        for (zonaMovFila; zonaMovFila < long; zonaMovFila++)
          arrayTableroCampos[poscol][zonaMovFila] = '<img src="img/rojo.png">';
        for (zonaMovFila = zonaMovFila - 1; zonaMovFila >= 0; zonaMovFila--)
          arrayTableroCampos[poscol][zonaMovFila] = '<img src="img/rojo.png">';

        // Añade la imagen de la REINA
        arrayTableroCampos[poscol][posfila] = '<img src="img/breina.png">';

      }
    }

    /* ==================================================================================================== */

    // CONDICIONES COLOR NEGRO - REY
    // Si es color 1 (negro) que ejecute este codigo
    if (color == 1) {

      // pieza 4 = REY
      if (pieza == 4) {

        if (zonaMovCol == 0) {

          arrayTableroCampos[zonaMovCol][zonaMovFila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][zonaMovFila] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][zonaMovFila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][zonaMovFila + 1] = '<img src="img/rojo.png">';

        } else if (zonaMovFila == 0 && zonaMovCol == 0) {

          arrayTableroCampos[zonaMovCol][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][zonaMovFila] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][zonaMovFila + 1] = '<img src="img/rojo.png">';

        } else if (zonaMovFila == 0 && zonaMovCol == 7) {

          arrayTableroCampos[zonaMovCol][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila + 1] = '<img src="img/rojo.png">';

        } else if (zonaMovCol == 7) {

          arrayTableroCampos[zonaMovCol][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol][zonaMovFila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila - 1] = '<img src="img/rojo.png">';

        } else if (zonaMovFila == 7 && zonaMovCol == 0) {

          arrayTableroCampos[areacol][areafila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[areacol + 1][areafila] = '<img src="img/rojo.png">';
          arrayTableroCampos[areacol + 1][areafila - 1] = '<img src="img/rojo.png">';

        } else if (zonaMovFila == 7 && zonaMovCol == 7) {

          arrayTableroCampos[zonaMovCol][zonaMovFila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila - 1] = '<img src="img/rojo.png">';

        } else {

          arrayTableroCampos[poscol][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[poscol][zonaMovFila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][posfila] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][posfila] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][zonaMovFila - 1] = '<img src="img/rojo.png">';

        }

        // Añade la imagen del REY
        arrayTableroCampos[poscol][posfila] = '<img src="img/nrey.png">';

      }

    // CONDICIONES COLOR BLANCO - REY
    // Si es color 1 (blanco) que ejecute este codigo
    } else if (color == 2) {
      if (pieza == 4) {
        
        
        if (zonaMovCol == 0) {

          arrayTableroCampos[zonaMovCol][zonaMovFila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][zonaMovFila] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][zonaMovFila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][zonaMovFila + 1] = '<img src="img/rojo.png">';

        } else if (zonaMovFila == 0 && zonaMovCol == 0) {

          arrayTableroCampos[zonaMovCol][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][zonaMovFila] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][zonaMovFila + 1] = '<img src="img/rojo.png">';

        } else if (zonaMovFila == 0 && zonaMovCol == 7) {

          arrayTableroCampos[zonaMovCol][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila + 1] = '<img src="img/rojo.png">';

        } else if (zonaMovCol == 7) {

          arrayTableroCampos[zonaMovCol][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol][zonaMovFila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila - 1] = '<img src="img/rojo.png">';

        } else if (zonaMovFila == 7 && zonaMovCol == 0) {

          arrayTableroCampos[areacol][areafila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[areacol + 1][areafila] = '<img src="img/rojo.png">';
          arrayTableroCampos[areacol + 1][areafila - 1] = '<img src="img/rojo.png">';

        } else if (zonaMovFila == 7 && zonaMovCol == 7) {

          arrayTableroCampos[zonaMovCol][zonaMovFila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila - 1] = '<img src="img/rojo.png">';

        } else {

          arrayTableroCampos[poscol][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[poscol][zonaMovFila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][posfila] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][posfila] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][zonaMovFila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][zonaMovFila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][zonaMovFila - 1] = '<img src="img/rojo.png">';

        }

        // Añade la imagen del REY
        arrayTableroCampos[poscol][posfila] = '<img src="img/brey.png">';
        
      }
    }

    /* ==================================================================================================== */

    // CONDICIONES COLOR NEGRO - PEON
    // Si es color 1 (negro) que ejecute este codigo
    if (color == 1) {

      // Pieza 5 = PEON
      if (pieza == 5) {

        if (zonaMovCol == 7){

          // Añade la imagen del PEON
          arrayTableroCampos[poscol][posfila] = '<img src="img/npeon.png">';

        } else {
          
          arrayTableroCampos[poscol][posfila] = '<img src="img/npeon.png">';
          arrayTableroCampos[zonaMovCol + 1][posfila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][posfila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol + 1][posfila] = '<img src="img/rojo.png">';

          // Añade la imagen del PEON
          arrayTableroCampos[poscol][posfila] = '<img src="img/npeon.png">';

        }
      }
    // CONDICIONES COLOR BLANCO - PEON
    // Si es color 1 (blanco) que ejecute este codigo
    } else if (color == 2) {

      // Pieza 5 = PEON
      if (pieza == 5) {

        if (zonaMovCol == 0) {

          // Añade la imagen del PEON
          arrayTableroCampos[poscol][posfila] = '<img src="img/bpeon.png">';

        } else {

          arrayTableroCampos[poscol][posfila] = '<img src="img/npeon.png">';
          arrayTableroCampos[zonaMovCol - 1][posfila - 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][posfila + 1] = '<img src="img/rojo.png">';
          arrayTableroCampos[zonaMovCol - 1][posfila] = '<img src="img/rojo.png">';

          // Añade la imagen del PEON
          arrayTableroCampos[poscol][posfila] = '<img src="img/bpeon.png">';

        }
      }
    }
      
    // DIBUJA LAS PIEZAS
    // Bucle que comienza a dibujar el tablero con las posiciones de amenaza y las piezas que se le indique en el menu de html
    for (var col=0; col < long; col++) {
      for (var fila=0; fila < long; fila++) {
        tablero.innerHTML += arrayTableroCampos[col][fila];
      }
    }     
  }
}